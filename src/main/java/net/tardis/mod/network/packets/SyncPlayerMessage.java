package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;

public class SyncPlayerMessage {
	
	public int id;
	public CompoundNBT data;
	
	public SyncPlayerMessage(int id, CompoundNBT data) {
		this.id = id;
		this.data = data;
	}
	
	public static void encode(SyncPlayerMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.id);
		buf.writeCompoundTag(mes.data);
	}
	
	public static SyncPlayerMessage decode(PacketBuffer buf) {
		return new SyncPlayerMessage(buf.readInt(), buf.readCompoundTag());
	}
	
	public static void handle(SyncPlayerMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			Entity result = Minecraft.getInstance().world.getEntityByID(mes.id);
			if(result != null)
				result.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> cap.deserializeNBT(mes.data));
		});
		cont.get().setPacketHandled(true);
	}

}
