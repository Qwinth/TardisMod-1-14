package net.tardis.mod.network.packets.data;

import net.minecraft.network.PacketBuffer;

public interface IData<T> {

    void encode(PacketBuffer buf);
    void decode(PacketBuffer buf);
    void apply(T object);
}
