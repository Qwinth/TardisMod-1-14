package net.tardis.mod.network.packets.exterior;

import net.tardis.mod.network.packets.data.IData;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

import java.util.function.Function;

public abstract class ExteriorData implements IData<ExteriorTile> {

    private Type type;

    public ExteriorData(Type type){
        this.type = type;
    }

    public Type getType(){
        return this.type;
    }

    public static enum Type{
        DOOR(DoorData::new),
        LIGHT(LightData::new),
        NAME(NameData::new),
        VARIANT(VariantData::new),
        INTERIOR_REGEN(InteriorRegenData::new),
        CRASHED(CrashedData::new);

        Function<Type, ExteriorData> provider;

        Type(Function<Type, ExteriorData> provider){
            this.provider = provider;
        }

        public ExteriorData provide(){
            return this.provider.apply(this);
        }
    }
}
