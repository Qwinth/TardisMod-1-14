package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.network.TPacketHandler;


public class VMTeleportMessage {

	public BlockPos pos;
	public boolean teleportPrecise;
	
	public VMTeleportMessage(BlockPos pos, boolean teleportPrecise) {
		this.pos = pos;
		this.teleportPrecise = teleportPrecise;
	}


	public static void encode(VMTeleportMessage mes,PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		buf.writeBoolean(mes.teleportPrecise);
	}
	
	public static VMTeleportMessage decode(PacketBuffer buf) {
		return new VMTeleportMessage(buf.readBlockPos(),buf.readBoolean());
	}
	
    public static void handle(VMTeleportMessage mes,  Supplier<NetworkEvent.Context> ctx){
        ctx.get().enqueueWork(()->{
    	    ServerPlayerEntity sender = ctx.get().getSender();
    	    TPacketHandler.handleVortexMTeleport(sender, mes.pos, mes.teleportPrecise); 			
        });
        ctx.get().setPacketHandled(true);
    }
    
}
