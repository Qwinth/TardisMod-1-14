package net.tardis.mod.network;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.world.dimensions.TDimensions;
/** Util class for handling server side packets*/
public class TPacketHandler {
    
    public static void handleVortexMTeleport(ServerPlayerEntity sender, BlockPos dest, boolean teleportPrecise) {
        if (!LandingSystem.isPosBelowOrAboveWorld(sender.world, dest.getY())
                 && LandingSystem.isBlockWithinWorldBorder(sender.getServerWorld(), dest.getX(), dest.getY(), dest.getZ()) 
                 && WorldHelper.canVMTravelToDimension(sender.world)) {
             double diff = Math.sqrt(sender.getPosition().distanceSq(dest)); //Calculates difference between dest and current pos
             float fuelDischarge = (float) (TConfig.SERVER.vmBaseFuelUsage.get() + (TConfig.SERVER.vmFuelUsageMultiplier.get() * diff)); //Scales discharge amount
             //This checks for which itemstack we want to open the container for
             //If we are holding a VM, use it as the itemstack. Otherwise, get the stack closest to slot 0
             ItemStack vm = new ItemStack(TItems.VORTEX_MANIP.get());
             ItemStack stack = PlayerHelper.getHeldOrNearestStack(sender, vm);
             
             stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> {
                     if (cap.getTotalCurrentCharge() >= fuelDischarge) { //Don't let them teleport if they don't have enough fuel to start the journey
                         cap.setDischargeAmount(fuelDischarge);
                         BlockPos destination = teleportPrecise ? dest : LandingSystem.getTopBlock(sender.world, dest); //Ensures the vm will tp you to a safe spot
                         SpaceTimeCoord coord = new SpaceTimeCoord(sender.world.getDimensionKey(), destination);
                        sender.getCapability(Capabilities.PLAYER_DATA).ifPresent(playerCap -> {
                            playerCap.setDestination(coord);
                            playerCap.calcDisplacement(sender.getPosition(), destination);
                        });
                        sender.getServerWorld().playSound(null, sender.getPosition(), TSounds.VM_TELEPORT.get(), SoundCategory.BLOCKS, 0.25F, 1F); //Play sound at start position
                        cap.setVmUsed(true); //Tell server that we used the VM to teleport into Vortex Dim
                        sender.getServer().enqueue(new TickDelayedTask(1, () -> {
                        	WorldHelper.teleportEntities(sender, sender.world.getServer().getWorld(TDimensions.VORTEX_DIM), new BlockPos(0, 500 + (0.5 * diff), 0), 0, 90); 
                        }));
                     }
                     else {
                         sender.sendStatusMessage(TextHelper.createVortexManipMessage(new TranslationTextComponent("message.vm.fuel_empty",fuelDischarge)), false);
                     }
                 }); 
             }
         else {
             sender.sendStatusMessage(TextHelper.createVortexManipMessage(new TranslationTextComponent("message.vm.invalidPos")), false);
         }
    }

}
