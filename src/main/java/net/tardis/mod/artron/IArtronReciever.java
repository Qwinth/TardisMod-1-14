package net.tardis.mod.artron;

public interface IArtronReciever {

    /**
     * Returns how much was accepted
     * @param amount
     * @return
     */
    float recieveArtron(float amount);
}
