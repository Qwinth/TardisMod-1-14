package net.tardis.mod.blocks;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.RiftCapability;
import net.tardis.mod.properties.Prop;

import java.util.List;
import java.util.Random;

public class XionCrystalBlock extends Block implements IWaterLoggable{

    public static final IntegerProperty TYPE = IntegerProperty.create("type", 0, 5);
    
    private static final VoxelShape shapeOne = VoxelShapes.create(0.15625, 0.0, 0.125, 0.84375, 0.125, 0.8125);
    private static final VoxelShape shapeTwo = getShapeTwo();
    private static final VoxelShape shapeThree = getShapeThree();
    private static final VoxelShape shapeFour = getShapeFour();
    private static final VoxelShape shapeFive = getShapeFive();
    private static final VoxelShape shapeSix = VoxelShapes.create(0.09375, 0.0, 0.09375, 0.90625, 0.14375, 0.90625);


    public XionCrystalBlock(){
        super(Prop.Blocks.BASIC_CRYSTAL.get().notSolid().variableOpacity().setLightLevel(state -> 15).tickRandomly());
        this.setDefaultState(this.getStateContainer().getBaseState().with(BlockStateProperties.WATERLOGGED, false));
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
    	int randomState = context.getWorld().rand.nextInt(5);
    	BlockState blockstate = context.getWorld().getBlockState(context.getPos());
    	boolean isWaterLogged = context.getWorld().getFluidState(context.getPos()).getFluid() == Fluids.WATER;
    	return this.getDefaultState().with(TYPE, randomState).with(BlockStateProperties.WATERLOGGED, isWaterLogged);
    }

    @Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		switch(state.get(TYPE)) {
		case 0:
			return shapeOne;
		case 1:
			return shapeTwo;
		case 2:
			return shapeThree;
		case 3:
			return shapeFour;
		case 4:
		    return shapeFive;
		case 5:
			return shapeSix;
		default:
			return shapeOne;
		}
	}

	@Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        super.onReplaced(state, worldIn, pos, newState, isMoving);

        if(state.getBlock() != newState.getBlock()){
            if(newState.getBlock() == TBlocks.xion_crystal.get()){
                worldIn.setBlockState(pos, getRandomCrystalState(worldIn.getRandom()));
            }
        }

    }

    public BlockState getRandomCrystalState(Random rand){
    	return this.getDefaultState().with(TYPE, rand.nextInt(TYPE.getAllowedValues().size()));
	}

    @Override
	public void randomTick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random) {
		super.randomTick(state, worldIn, pos, random);

		if(random.nextInt(50) == 7){
			worldIn.getChunkAt(pos).getCapability(Capabilities.RIFT).ifPresent(cap ->{
				if(cap.isRift() && cap.getRiftEnergy() > 100.0F){


					List<BlockPos> getPosesAround = Lists.newArrayList();

					//Gather places in range of this Crystal
					int range = 1;
					for(int x = -range; x <= range; ++x){
						for(int y = -range; y <= range; ++y) {
							for (int z = -range; z <= range; ++z) {
								getPosesAround.add(new BlockPos(pos.getX() + x, pos.getY() + y, pos.getZ() + z));
							}
						}
					}

					//Find a random valid pos

					while(!getPosesAround.isEmpty()){

						//Get a position and remove it
						int index = random.nextInt(getPosesAround.size());
						BlockPos newPos = getPosesAround.get(index);
						getPosesAround.remove(index);

						boolean isValid = worldIn.isAirBlock(newPos) && worldIn.getBlockState(newPos.down()).isSolid();

						if(isValid){
							worldIn.setBlockState(newPos, getRandomCrystalState(random));
							return; //Break out of loop
						}

					}
				}

			});
		}

	}

	@Override
	public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
		super.neighborChanged(state, worldIn, pos, blockIn, fromPos, isMoving);

		if(!worldIn.isRemote && worldIn.isAirBlock(pos.down())){
			worldIn.destroyBlock(pos, true);
		}
	}

	@Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder.add(TYPE, BlockStateProperties.WATERLOGGED));
    }
    
    @Override
	public FluidState getFluidState(BlockState state) {
    	return state.get(BlockStateProperties.WATERLOGGED) ? Fluids.WATER.getStillFluidState(false) : super.getFluidState(state);
	}

	private static VoxelShape getShapeTwo() {
    	VoxelShape shape = VoxelShapes.create(0.0625, 0.0, 0.396446875, 0.4375, 0.5625, 0.646446875);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.4375, 0.0, 0.208946875, 0.625, 0.125, 0.396446875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.4375, 0.0, 0.521446875, 0.6875, 0.125, 0.771446875), IBooleanFunction.OR);
        return shape;
    }
    
    private static VoxelShape getShapeThree() {
    	VoxelShape shape = VoxelShapes.create(0.375, 0.0, 0.333946875, 0.6875, 0.125, 0.646446875);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.1875, 0.0, 0.396446875, 0.375, 0.375, 0.521446875), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.6875, 0.0, 0.583946875, 0.875, 0.25, 0.708946875), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.375, 0.0, 0.208946875, 0.625, 0.3125, 0.333946875), IBooleanFunction.OR);
        return shape;
    }
    
    private static VoxelShape getShapeFour() {
    	VoxelShape shape = VoxelShapes.create(0.1875, 0.0, 0.396446875, 0.4375, 0.4375, 0.521446875);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.625, 0.0, 0.583946875, 0.875, 0.4375, 0.708946875), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.375, 0.0, 0.208946875, 0.625, 0.4375, 0.333946875), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.4375, 0.0, 0.3125, 0.6875, 0.0625, 0.625), IBooleanFunction.OR);
    	return shape;
    }
    
    private static VoxelShape getShapeFive() {
    	VoxelShape shape = VoxelShapes.create(0.4375, 0.0, 0.5, 0.75, 0.125, 0.8125);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.25, 0.0, 0.333946875, 0.5, 0.4375, 0.583946875), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.15625, 0.0, 0.208946875, 0.40625, 0.1875, 0.458946875), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.125, 0.0, 0.521446875, 0.3125, 0.125, 0.646446875), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.4375, 0.0, 0.271446875, 0.625, 0.25, 0.396446875), IBooleanFunction.OR);
    	return shape;
    }
}
