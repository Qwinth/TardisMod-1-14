package net.tardis.mod.cap;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants.NBT;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.missions.MiniMissionType;
import net.tardis.mod.registries.MissionRegistry;

public class MissionWorldCapability implements IMissionCap {

    private World world;
    private List<MiniMission> missions = Lists.newArrayList();

    public MissionWorldCapability(World world) {
        this.world = world;
    }

    @Nullable
    @Override
    public MiniMission getMissionForPos(BlockPos pos) {
        for (MiniMission mission : missions) {
            if (mission.getPos().withinDistance(pos, mission.getRange()))
                return mission;
        }
        return null;
    }

    @Override
    public void addMission(MiniMission mission) {
        this.missions.add(mission);
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();

        ListNBT list = new ListNBT();
        for (MiniMission mis : this.missions) {
            CompoundNBT misTag = mis.serializeNBT();
            misTag.putString("registry_name", mis.getType().getRegistryName().toString());
            list.add(misTag);
        }
        tag.put("missions", list);

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {

        this.missions.clear();
        ListNBT list = tag.getList("missions", NBT.TAG_COMPOUND);
        for (INBT nbt : list) {
            CompoundNBT comp = (CompoundNBT) nbt;
            ResourceLocation key = new ResourceLocation(comp.getString("registry_name"));
            MiniMissionType missionType = MissionRegistry.MISSION_REGISTRY.get().getValue(key);
            if (missionType != null) {
                MiniMission mission = missionType.create(world, BlockPos.ZERO, 0);
                mission.deserializeNBT(comp);
                this.missions.add(mission);
            }
        }

    }

    @Override
    public void tick(World world) {
    	if (!world.isRemote()) {
    		final ServerWorld serv = (ServerWorld) world;
            for(MiniMission m : this.missions) {
    			
    			if(m instanceof ITickableMission)
    				((ITickableMission)m).tick(serv);
    				
    			if(!world.isRemote) {
    				world.getServer().enqueue(new TickDelayedTask(1, () -> {
    					
    					//Remove players
    					for(UUID entID : Lists.newArrayList(m.getTrackingPlayers())) {
    						PlayerEntity ent = world.getServer().getPlayerList().getPlayerByUUID(entID);
    						if(ent instanceof ServerPlayerEntity) {
    							if(m.isMissionComplete() || !ent.isAlive() || !m.isInsideArea(ent)) {
    								if (m.getTrackingPlayers().contains(ent.getUniqueID())) {
    								    m.removeTrackingPlayer((ServerPlayerEntity)ent);
    								}
    							}
    						}
    					}
    					
    					//Add player
    					for(ServerPlayerEntity player : serv.getPlayers()) {
    						//If not tracking
    						if(!m.getTrackingPlayers().contains(player.getUniqueID())) {
    							//If in range and not complete
    							if(!m.isMissionComplete() && m.isInsideArea(player)) {
    								m.addTrackingPlayer(player);
    							}
    						}
    					}
    					
    				}));
    			}
    		}
    	}
    }

    /**
     * Read-only
     */
    @Override
    public List<MiniMission> getAllMissions() {
        return Collections.unmodifiableList(this.missions);
    }
    
    public void setMissions(List<MiniMission> missions) {
		this.missions.clear();
		this.missions.addAll(missions);
	}


}
