package net.tardis.mod.entity.hostile.dalek.weapons;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShieldItem;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.entity.projectiles.LaserEntity;
import net.tardis.mod.misc.AbstractWeapon;

public class DalekWeapon extends AbstractWeapon<DalekEntity>{
	private DalekEntity dalek;

	public DalekWeapon(DalekEntity dalek){
		this.dalek = dalek;
	}

	@Override
	public void onHit(LaserEntity laser, RayTraceResult result) {
		if (!this.dalek.world.isRemote()) {
			if (result.getType() == RayTraceResult.Type.ENTITY) { //Handle tool behaviour if the dalek somehow gets equipped tools in the future
	            EntityRayTraceResult entityHitResult = ((EntityRayTraceResult) result);
	            Entity hitEntity = entityHitResult.getEntity();
	            float extraAttackDamage = 0.0F;
	            float knockbackAmount = (float)this.dalek.getAttributeValue(Attributes.ATTACK_KNOCKBACK);
	            if (hitEntity instanceof LivingEntity) {
	            	LivingEntity livingTarget = (LivingEntity)hitEntity;
	            	if (this.dalek.getHeldItemMainhand() != null) {
	            		extraAttackDamage += EnchantmentHelper.getModifierForCreature(this.dalek.getHeldItemMainhand(), ((LivingEntity)hitEntity).getCreatureAttribute());
	            		knockbackAmount += (float)EnchantmentHelper.getKnockbackModifier(this.dalek);
	                	int setOnFireTime = EnchantmentHelper.getFireAspectModifier(this.dalek);
	                    if (setOnFireTime > 0) {
	                       hitEntity.setFire(setOnFireTime * 4);
	                    }
	                    this.dalek.attackEntityFrom(dalek.getDalekType().getDamageSource(dalek), extraAttackDamage);
	            	}
	            	else {
	            		if (knockbackAmount > 0) {
	            			livingTarget.applyKnockback(knockbackAmount * 0.5F, (double)MathHelper.sin(this.dalek.rotationYaw * ((float)Math.PI / 180F)), (double)(-MathHelper.cos(this.dalek.rotationYaw * ((float)Math.PI / 180F))));
	            			this.dalek.setMotion(this.dalek.getMotion().mul(0.6D, 1.0D, 0.6D));
	                	}
	            		if (livingTarget instanceof PlayerEntity) {
	                        PlayerEntity playerentity = (PlayerEntity)livingTarget;
	                        this.dalek.maybeDisableShield(playerentity, this.dalek.getHeldItemMainhand(), playerentity.isHandActive() ? playerentity.getActiveItemStack() : ItemStack.EMPTY);
	                     }
	            	}
	                
	            }
	        }
		}
		
	}

	@Override
	public float damage() {
		return (float)this.dalek.getAttribute(Attributes.ATTACK_DAMAGE).getBaseValue();
	}

	@Override
	public boolean useWeapon(DalekEntity dalek) {
		if (!dalek.world.isRemote) {
			LaserEntity laser = new LaserEntity(TEntities.LASER.get(), dalek, dalek.world, damage(), dalek.getDalekType().getDamageSource(dalek));
	    	//Setup laser fire direction, velocity and inaccuracy
	    	laser.setDirectionAndMovement(laser, dalek.rotationPitch, dalek.rotationYawHead, 0.0F, 2F, 0.1F);
	    	laser.setColor(dalek.getDalekType().getLaserColour());
	    	Entity targetEntity = dalek.getAttackTarget();
	    	if (targetEntity != null) {
	    		double distToTarget = dalek.getDistanceSq(targetEntity.getPosX(), targetEntity.getPosY(), targetEntity.getPosZ()) / 35;
	    		laser.setRayLength((float)distToTarget);
	    	}
	    	laser.setWeaponType(this);
	    	dalek.world.addEntity(laser);
	    	return true;
		}
		else return false;
	}

	@Override
	public void onHitEntityPre(LaserEntity laserEntity, Entity result) {
		super.onHitEntityPre(laserEntity, result);
		if (!dalek.world.isRemote) {
			if (result instanceof PlayerEntity) {
	            PlayerEntity player = (PlayerEntity)result;
	            ItemStack heldItem = player.getHeldItemOffhand().getItem() instanceof ShieldItem ? player.getHeldItemOffhand() : player.getHeldItemMainhand();
	            this.dalek.maybeDisableShield(player, heldItem);
	        }
		}
	}
	
	

}
