package net.tardis.mod.entity.ai;

import net.minecraft.entity.ai.goal.OpenDoorGoal;
import net.minecraft.entity.monster.AbstractRaiderEntity;

public class TardisRaiderOpenGoal extends OpenDoorGoal {
	private AbstractRaiderEntity raider;
	
    public TardisRaiderOpenGoal(AbstractRaiderEntity raider) {
       super(raider, false);
       this.raider = raider;
    }

    /**
     * Returns whether execution should begin. You can also read and cache any state necessary for execution in this
     * method as well.
     */
    public boolean shouldExecute() {
       return super.shouldExecute() && this.raider.isRaidActive();
    }
 }
