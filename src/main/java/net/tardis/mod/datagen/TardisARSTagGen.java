package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.helper.Helper;

public class TardisARSTagGen extends TardisItemTagGen {
    
    protected List<Block> otherTaggedBlocks = new ArrayList<>();

    public TardisARSTagGen(DataGenerator gen){
        super(gen);
    }

    @Override
    public void registerTags() {}

    @Override
    public void act(DirectoryCache cache, Path path) throws IOException {
        List<ResourceLocation> items = Lists.newArrayList();
        populateOtherTaggedList();
        for (Block blockEntry : ForgeRegistries.BLOCKS) {
            if (blockEntry instanceof IARS || otherTaggedBlocks.contains(blockEntry)) {
                items.add(blockEntry.getRegistryName());
            }
        }
        
        this.generateTable(cache, getPath(path, Helper.createRL("ars")), () -> this.serialize(items));
    }
    
    protected void populateOtherTaggedList() {
        otherTaggedBlocks.add(TBlocks.blinking_lights.get());
        otherTaggedBlocks.add(TBlocks.steam_grate.get());
        otherTaggedBlocks.add(TBlocks.alabaster_bookshelf.get());
        otherTaggedBlocks.add(TBlocks.ebony_bookshelf.get());
        otherTaggedBlocks.add(TBlocks.blue_pillar.get());
        otherTaggedBlocks.add(TBlocks.foam_pipes.get());
        
        otherTaggedBlocks.add(TBlocks.alabaster.get());
        otherTaggedBlocks.add(TBlocks.alabaster_wall.get());
        
        otherTaggedBlocks.add(TBlocks.tungsten.get());
        
        otherTaggedBlocks.add(TBlocks.tungsten_pattern.get());
        otherTaggedBlocks.add(TBlocks.tungsten_pattern_slab.get());
        otherTaggedBlocks.add(TBlocks.tungsten_pattern_stairs.get());
        
        otherTaggedBlocks.add(TBlocks.tungsten_plate.get());
        otherTaggedBlocks.add(TBlocks.tungsten_plate_small.get());
        otherTaggedBlocks.add(TBlocks.tungsten_plate_slab.get());
        otherTaggedBlocks.add(TBlocks.tungsten_plate_stairs.get());
        
        otherTaggedBlocks.add(TBlocks.tungsten_pipes.get());
        otherTaggedBlocks.add(TBlocks.tungsten_pipes_slab.get());
        otherTaggedBlocks.add(TBlocks.tungsten_pipes_stairs.get());
        
        otherTaggedBlocks.add(TBlocks.tungsten_screen.get()); 
    }

    @Override
    public String getName() {
        return "TARDIS ARS Item Tag";
    }

}
