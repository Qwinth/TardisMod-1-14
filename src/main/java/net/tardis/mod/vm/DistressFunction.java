package net.tardis.mod.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TardisNameGuiMessage;


/**
 * Created by 50ap5ud5
 * on 9 Jul 2020 @ 7:56:02 pm
 */
public class DistressFunction extends BaseVortexMFunction {
    @Override
    public void sendActionOnClient(World world, PlayerEntity player) {
        if (world.isRemote && WorldHelper.canVMTravelToDimension(world)) {
            ClientHelper.openGUI(TardisConstants.Gui.VORTEX_DISTRESS, null);
            super.sendActionOnClient(world, player);
        } else {
        	player.sendStatusMessage(TextHelper.createVortexManipMessage(TardisConstants.Translations.VM_FORBIDDEN_USE), false);
        }
    }
    
    @Override
    public void sendActionOnServer(World world, ServerPlayerEntity player) {
    	Network.sendTo(TardisNameGuiMessage.create(player.getServer()), player);
    }

    @Override
    public VortexMUseType getLogicalSide() {
        return VortexMUseType.BOTH;
    }

}
