package net.tardis.mod.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;


public class TeleportFunction extends BaseVortexMFunction {

    @Override
    public void sendActionOnClient(World world, PlayerEntity player) {
        if (world.isRemote && WorldHelper.canVMTravelToDimension(world)) {
            ClientHelper.openGUI(TardisConstants.Gui.VORTEX_TELE, null);
            super.sendActionOnClient(world, player);
        } else {
            player.sendStatusMessage(TextHelper.createVortexManipMessage(TardisConstants.Translations.VM_FORBIDDEN_USE), false);
        }
    }

    @Override
    public VortexMUseType getLogicalSide() {
        return VortexMUseType.CLIENT;
    }
}
