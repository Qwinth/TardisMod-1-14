package net.tardis.mod.client.guis.monitors;

import com.mojang.blaze3d.matrix.MatrixStack;

public interface IMonitorGui {

    void renderMonitor(MatrixStack matrixStack);

    int getMinY();

    int getMinX();

    int getMaxX();

    int getMaxY();

    int getWidth();

    int getHeight();

    int getGuiID();
}
