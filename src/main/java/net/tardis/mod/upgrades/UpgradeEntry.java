package net.tardis.mod.upgrades;

import javax.annotation.Nullable;

import net.minecraft.item.Item;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class UpgradeEntry extends ForgeRegistryEntry<UpgradeEntry>{

	private IConsoleSpawner<Upgrade> spawn;
	private Item item;
	private Class<? extends Subsystem> system;
	
	/**
	 * 
	 * @param spawn - Functional Interface to create your Upgrade
	 * @param item - Item key
	 * @param system - Parent system, for those that depend on it. Can be null
	 */
	public UpgradeEntry(IConsoleSpawner<Upgrade> spawn, Item item, @Nullable Class<? extends Subsystem> system) {
		this.spawn = spawn;
		this.item = item;
		this.system = system;
	}
	
	public Item getItem() {
		return this.item;
	}
	
	public Upgrade create(ConsoleTile tile) {
		Upgrade upgrade = spawn.create(this, tile, system);
		return upgrade;
	}
	
	public static interface IConsoleSpawner<T>{
		T create(UpgradeEntry entry, ConsoleTile console, @Nullable Class<? extends Subsystem> sys);
	}

}
