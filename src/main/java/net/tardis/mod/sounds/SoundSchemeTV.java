package net.tardis.mod.sounds;

public class SoundSchemeTV extends SoundSchemeBase{

	public SoundSchemeTV() {
		super(() -> TSounds.TAKEOFF_TV.get(), () -> TSounds.LAND_TV.get(), () -> TSounds.FLY_LOOP_TV.get());
	}

	@Override
	public int getLandTime() {
		return 240;
	}

	@Override
	public int getTakeoffTime() {
		return 240;
	}

	@Override
	public int getLoopTime() {
		return 42;
	}

}
