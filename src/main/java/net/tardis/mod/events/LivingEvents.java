package net.tardis.mod.events;

import net.minecraft.entity.LivingEntity;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.Cancelable;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class LivingEvents {

	
	@Cancelable
	public static class SpaceAir extends LivingEvent{

		public SpaceAir(LivingEntity entity) {
			super(entity);
		}
		
	}
	
	public static class TardisEnterEvent extends LivingEvent{

		private final World leaving;
		private final ExteriorTile exterior;
		
		public TardisEnterEvent(LivingEntity entity, ExteriorTile tile, World leaving) {
			super(entity);
			this.leaving = leaving;
			this.exterior = tile;
		}
		
		public ExteriorTile getExterior() {
			return this.exterior;
		}

		public World getLeaving() {
			return leaving;
		}
	}
	
	public static class TardisLeaveEvent extends LivingEvent{

		private final World entering;
		private final DoorEntity door;
		
		
		public TardisLeaveEvent(LivingEntity entity, DoorEntity door, World entering) {
			super(entity);
			this.door = door;
			this.entering = entering;
		}
		
		public World getEntering() {
			return entering;
		}


		public DoorEntity getDoor() {
			return door;
		}
	}
}
