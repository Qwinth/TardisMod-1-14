package net.tardis.api.space.cap;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.tardis.api.space.IOxygenSealer;
import net.tardis.api.space.thread.OxygenThread;

import java.util.ArrayList;
import java.util.List;

public class OxygenSealerCapability implements IOxygenSealer {

    private OxygenThread sealerThread;
    private List<BlockPos> sealedPoses = new ArrayList<>();
    private int lastChecked = 0;
    private final int maxCheckTime;

    public OxygenSealerCapability(int maxCheckTime){
        this.maxCheckTime = maxCheckTime;
    }

    public void findsealedPos(World world, BlockPos pos) {
        if(!world.isRemote) {
            if(this.sealerThread == null || !this.sealerThread.isAlive()) {

                this.sealerThread = new OxygenThread(world, pos, this, 2700);
                this.sealerThread.start();
            }
        }
        this.lastChecked = this.maxCheckTime;
    }

    @Override
    public void setSealedPositions(List<BlockPos> list) {
        this.sealedPoses.clear();
        this.sealedPoses.addAll(list);
        this.lastChecked = 100;
    }

    @Override
    public List<BlockPos> getSealedPositions() {
        return this.sealedPoses;
    }

    @Override
    public void tick(World world, BlockPos pos) {
        if(!world.isRemote) {
            if(this.lastChecked > 0)
                --this.lastChecked;
            else this.findsealedPos(world, pos);
        }
    }
}
